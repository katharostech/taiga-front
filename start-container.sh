#!/bin/sh

# Configure nginx with the passed-in environment variables
/configure-nginx.sh

# Configure taiga with the passed-in environment variables
/configure-taiga.sh

# Start nginx
nginx
