#!/bin/sh

# Set default values
# These default values aren't going to work but at least they won't be blank
BACKEND_HOST=${BACKEND_HOST-taiga-back}
BACKEND_PORT=${BACkEND_PORT-80}

# Subsitute the ${BACKEND_HOST} and ${BACKEND_PORT} values in the nginx config
# file with the environment variables.
sed -i s/\${BACKEND_HOST}/${BACKEND_HOST}/ /etc/nginx/conf.d/taiga-front.conf
sed -i s/\${BACKEND_PORT}/${BACKEND_PORT}/ /etc/nginx/conf.d/taiga-front.conf
