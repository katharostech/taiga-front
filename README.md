# Taiga Front Docker Image

This is a Docker image for the Taiga project management tool frontend.

## Environment Variables

### BACKEND_HOST

The hostname of the taiga-back instance to forward api traffic to.

**Default:** `taiga-back`

### BACKEND_PORT

The port of the taiga-back instance to forward api traffic to.

**Default:** `80`

### TAIGA_THEMES

A space delimited list of enabled Taiga themes. These must be installed at `/usr/share/taiga-front/dist/app/themes/`.

**Default:** `taiga`

### TAIGA_DEFAULT_THEME

The name of the default theme that Taiga will use.

**Default:** `taiga`

## Example Run Command

```bash
docker run \
-d -p 80:80 \
--name taiga-front \
-e BACKEND_HOST=taiga-back \
-e BACKEND_PORT=80 \
katharostech/taiga-front
```
