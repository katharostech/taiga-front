FROM alpine

# Update OS
RUN apk upgrade --no-cache

# Install dependencies
RUN apk add --no-cache \
  ca-certificates \
  nginx \
  wget

# Create required nginx run directory
RUN mkdir -p /run/nginx

# Remove default nginx config
RUN rm /etc/nginx/conf.d/default.conf
RUN rm /etc/nginx/nginx.conf

# Copy in our nginx config
COPY taiga-front.conf /etc/nginx/conf.d/taiga-front.conf
COPY nginx.conf /etc/nginx/nginx.conf

# Collect the taiga-front version to download from github as a build argument
# and save it in an environment variable
ARG TAIGA_FRONT_VERSION
ENV TAIGA_FRONT_VERSION=$TAIGA_FRONT_VERSION

# Download and install the taiga-front-dist from the github release
RUN \
  mkdir -p /usr/share/taiga-front/ && \
  cd /usr/share/taiga-front/ && \
  wget https://github.com/taigaio/taiga-front-dist/archive/${TAIGA_FRONT_VERSION}.tar.gz && \
  tar -xzf ${TAIGA_FRONT_VERSION}.tar.gz && \
  rm ${TAIGA_FRONT_VERSION}.tar.gz && \
  mv taiga-front-dist-${TAIGA_FRONT_VERSION}/* . && \
  rm -r taiga-front-dist-${TAIGA_FRONT_VERSION}

# Copy in our taiga config template
COPY conf.json /usr/share/taiga-front/dist/

# Copy in our Docker cmd
COPY docker-cmd.sh /docker-cmd.sh
RUN chmod 744 /docker-cmd.sh

# Copy in the Nginx configuration script
COPY configure-nginx.sh /configure-nginx.sh
RUN chmod 744 /configure-nginx.sh

# Copy in the Taiga configuration script
COPY configure-taiga.sh /configure-taiga.sh
RUN chmod 744 /configure-taiga.sh

# Copy in the container-start and container-stop scripts
COPY start-container.sh /start-container.sh
RUN chmod 744 /start-container.sh

COPY stop-container.sh /stop-container.sh
RUN chmod 744 /stop-container.sh

# Set the Docker command
CMD ["/docker-cmd.sh"]
