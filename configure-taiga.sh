#!/bin/sh

conf_file=/usr/share/taiga-front/dist/conf.json

# Set default values
TAIGA_THEMES=${TAIGA_THEMES-"taiga"}
TAIGA_DEFAULT_THEME=${TAIGA_DEFAULT_THEME-"taiga"}

# Generate json formatted list from the TAIGA_THEMES, space delimited, list
theme_string=""
for theme in $TAIGA_THEMES; do
    if [ -z $theme_string ]; then
        theme_string="['$theme'"
    else
        theme_string="$theme_string,'$theme'"
    fi
done
theme_string="$theme_string]"

sed -i s/\${TAIGA_THEMES}/${theme_string}/ $conf_file
sed -i s/\${TAIGA_DEFAULT_THEME}/${TAIGA_DEFAULT_THEME}/ $conf_file
